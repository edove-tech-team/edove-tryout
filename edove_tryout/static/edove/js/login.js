windowHeight = $(window).height();
navHeight = $('nav').outerHeight()
$("#decorator-auth").css("height", (windowHeight-navHeight) + "px");
$("#decorator-auth").css("width",  + ($(window).width()*0.6) + "px");

$(window).resize(function (){
    windowHeight = $(window).height();
    $("#decorator-auth").css("height", (windowHeight-navHeight) + "px");
    $("#decorator-auth").css("width",  + ($(window).width()*0.6) + "px");
    $('.form-body').css("width", $('.form-headline').width()+"px");
    $('.btn-submit').css("width", $('.form-headline').width()+"px");
})

$(document).ready(function() {
    $(".form-section").css("margin-top", navHeight+ "px");
    $('.form-body').css("width", $('.form-headline').width()+"px");
    $('.btn-submit').css("width", $('.form-headline').width()+"px");
});