from django.shortcuts import render

# Create your views here.
def index(request):
    context = {'current':"home"}
    return render(request, 'edove/home.html', context)