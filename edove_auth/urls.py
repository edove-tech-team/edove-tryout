from django.urls import include, path
from edove_auth.views import index, signup, profile, login_view, logout_view

urlpatterns = [
    path('daftar/', signup, name="signup"),
    path('profil/', profile, name="profile"),
    path('masuk/', login_view, name="masuk"),
    path('keluar/', logout_view, name="logout")
]
