from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from edove_auth.managers import EdoveUserManager


class EdoveUser(AbstractBaseUser, PermissionsMixin):
    nama_lengkap = models.CharField(_("nama lengkap"), max_length=255, default="")
    email = models.EmailField(_("email"), max_length=255, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = EdoveUserManager()

    class Meta:
        ordering = ['email']
        db_table = "auth_user"


class EdoveSiswa(EdoveUser):
    asal_sekolah = models.CharField(_("asal sekolah"), max_length=255)
    kelas = models.IntegerField(_("kelas"))
    asal_kota = models.CharField(_("asal kota"), max_length=255)
    asal_provinsi = models.CharField(_("asal provinsi"), max_length=255)
    universitas_tujuan = models.CharField(_("universitas tujuan"), max_length=255)
    jurusan_1 = models.CharField(_("pilihan jurusan 1"), max_length=50)
    jurusan_2 = models.CharField(_("pilihan jurusan 2"), max_length=50)

    class Meta:
        verbose_name = "siswa"
        verbose_name_plural = "siswas"
        permissions = [('can_do_tryout', 'Can do tryout')]
