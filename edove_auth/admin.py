from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from edove_auth.forms import EdoveUserCreationForm, EdoveUserChangeForm, EdoveSiswaSignUpForm
from edove_auth.models import EdoveUser, EdoveSiswa
from django.contrib.auth.models import Group

class EdoveUserAdmin(UserAdmin):
    add_form = EdoveUserCreationForm
    form = EdoveUserChangeForm
    model = EdoveUser

    list_display = ('nama_lengkap', 'email', 'is_superuser', 'is_staff',)
    list_filter = ('nama_lengkap', 'email', 'is_superuser', 'is_staff',)

    fieldsets = (
        (None, {'fields': ('nama_lengkap', 'email', 'password')}),
        ('Permissions', {'fields': ('is_superuser', 'is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nama_lengkap', 'email', 'password1', 'password2', 'is_superuser', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(EdoveUser, EdoveUserAdmin)
admin.site.register(EdoveSiswa)

admin.site.unregister(Group)