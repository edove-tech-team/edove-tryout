from django.apps import AppConfig


class EdoveAuthConfig(AppConfig):
    name = 'edove_auth'
