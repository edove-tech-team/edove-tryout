from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserChangeForm, UserCreationForm

from edove_auth.models import EdoveUser, EdoveSiswa


class EdoveUserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs = {
                    'class':'form-control form-unit',
                    'name':'password1'
                }))
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput(attrs = {
                    'class':'form-control form-unit',
                    'name':'password2'
                }))

    class Meta:
        model = EdoveUser
        fields = ('nama_lengkap', 'email', )
    
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class EdoveUserChangeForm(UserChangeForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = EdoveUser
        fields = ('nama_lengkap', 'email', 'password', 'is_superuser', 'is_active', 'is_staff')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    class Meta:
        model = EdoveUser
        fields = ('nama_lengkap', 'email', )


class EdoveSiswaSignUpForm(UserCreationForm):
    class Meta:
        model = EdoveSiswa

        fields = (
            'nama_lengkap',
            'email',
            'asal_sekolah',
            'kelas',
            'asal_kota',
            'asal_provinsi',
            'universitas_tujuan',
            'jurusan_1',
            'jurusan_2',
            'password1',
            'password2'
        )

        labels = {
            'nama_lengkap': 'Nama Lengkap',
            'asal_sekolah':'Asal Sekolah',
            'kelas':'Kelas',
            'asal_kota':'Asal Kota',
            'asal_provinsi':'Asal Provinsi',
            'universitas_tujuan': 'Universitas Tujuan',
            'jurusan_1': 'Universitas Tujuan',
            'jurusan_2': '',
            'password1': 'Kata Sandi',
            'password2': 'Konfirmasi Kata Sandi'
        }

        widgets = {
            'nama_lengkap':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Edove Indonesia',
                    'name':'nama_lengkap'
                }
            ),
            'email':forms.EmailInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Edove Indonesia',
                    'name':'email'
                }
            ),
            'asal_sekolah':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: SMAN 1 Jakarta',
                    'name':'asal_sekolah'
                }
            ),
            'kelas':forms.NumberInput(
                attrs = {
                    'class':'form-control form-unit',
                    'min':'10',
                    'max':'12',
                    'name':'kelas',
                    'placeholder':'misal: 12'
                }
            ),
            'universitas_tujuan':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Universitas X',
                    'name':'universitas_tujuan'
                }
            ),
            'asal_kota':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Jakarta',
                    'name':'asal_kota'
                }
            ),
            'asal_provinsi':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Jawa Barat',
                    'name':'asal_provinsi'
                }
            ),
            'jurusan_1':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Kedokteran',
                    'name':'jurusan_1'
                }
            ),
            'jurusan_2':forms.TextInput(
                attrs = {
                    'class':'form-control form-unit',
                    'placeholder':'misal: Gizi',
                    'name':'jurusan_2'
                }
            ),
            'password1':forms.PasswordInput(
                attrs = {
                    'class':'form-unit',
                }
            ),
            'password2':forms.PasswordInput(
                attrs = {
                    'class':'form-unit',
                }
            ),
        }

class EdoveLoginForm(forms.Form):
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control form-unit',
        'name':'email',
        'placeholder':'Masukkan email anda'
    }), required=True)

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class':'form-control form-unit',
        'name':'password',
        'placeholder':'Masukkan password anda'
    }), required=True)