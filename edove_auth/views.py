from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required


from edove_auth.forms import EdoveUserCreationForm, EdoveSiswaSignUpForm, EdoveLoginForm
from edove_auth.models import EdoveSiswa


# Create your views here.
def index(request):
    context = {}
    return render(request, 'edove_auth/auth.html', context)


def signup(request):
    context = {
    }

    if request.method == 'POST':
        # 1st Step : Create Edove User and Login
        # nama_lengkap = request.POST['nama_lengkap']
        # email = request.POST['email']
        # password1 = request.POST['password1']
        # password2 = request.POST['password2']
        # form = EdoveUserCreationForm({
        #     'csrfmiddlewaretoken': request.POST['csrfmiddlewaretoken'],
        #     'name_lengkap': ''
        # })

        # 2nd Step : Create Edove Siswa
        form = EdoveSiswaSignUpForm(request.POST)
        print(form.errors)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=email, password=raw_password)
            login(request, user)
            return redirect('profile')
    else:
        form = EdoveSiswaSignUpForm()
    context['form'] = form

    return render(request, 'edove_auth/registration.html', context)

def login_view(request):
    if(request.method == 'POST'):
        form = EdoveLoginForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(username=email, password=password)
            login(request, user)
            return redirect('profile')
    else:
        form = EdoveLoginForm()
    return render(request, 'registration/login.html', {'form':form, 'current':'login'})

@login_required
def profile(request):
    context = {}

    return render(request, 'edove_auth/profile.html', context)

def logout_view(request):
    logout(request)
    return redirect("/")