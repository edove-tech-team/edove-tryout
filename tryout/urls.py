from django.urls import path
from tryout.views import index

app_name = 'tryout'

urlpatterns = [
    path('', index, name='index')
]
