from django.urls import path
from tryout_setup.views import index

app_name = 'tryout_setup'

urlpatterns = [
    path('', index, name='index')
]
