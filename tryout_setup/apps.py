from django.apps import AppConfig


class TryoutSetupConfig(AppConfig):
    name = 'tryout_setup'
